import React from 'react'
import zxcvbn from 'zxcvbn'
const passwordStrengthIndicator = ({password1}) =>{
	const testResult = zxcvbn(password1);
	const num = testResult.score*100/4;	
		const CreatePasswordLabel =() =>{
		switch(testResult.score){
			case 0:
				return 'Very Weak'	
			case 1:
	 			return 'Weak'
			case 2:
				return 'Fair'
			case 3:
				return 'Good'
			case 4:
				return 'Strong'
			default:
				return ' '


		}
	}
	const functionProgressColor =() =>{
		switch(testResult.score){
			case 0:
				return '#828282'	
			case 1:
	 			return '#EA1111'
			case 2:
				return '#FFAD00'
			case 3:
				return '#9BC158'
			case 4:
				return '#00B500'
			default:
				return 'none'


		}
	}
	const changePasswordColor = () =>({
	width:`${num}%`,
	background: functionProgressColor(),
	height: '7px'

	})
return (


	<>
		<div className='progress'>
			<div className='progress-bar' style={changePasswordColor()}></div>
		</div >
		 <p className= 'text-right'style={{color: functionProgressColor()}} >{CreatePasswordLabel() }</p>
	</>

	)
}
export default passwordStrengthIndicator